package templatemethod;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber extends Game {
	
	Random rand;
	Scanner scanner = new Scanner(System.in);
	boolean end;
	int winner;
	int pick;
	
	@Override
	void initializeGame() {
		rand = new Random();
		pick = rand.nextInt(100)+1;
		end = false;
		System.out.println("Game starts!\n\n");
	}

	@Override
	void makePlay(int player) {
	
			System.out.println("Next in player number " + player + "\n Guess the number between 0-100!");
			int guess = scanner.nextInt();
			
			if(guess == pick) {
				winner = player;
				end = true;
				System.out.println("WOHOOO you guessed it right! The number was " + pick);
			} else if(guess > pick) {
				System.out.println("That's too big, guess again!");
			} else {
				System.out.println("That's too small, guess again!");
			}
			System.out.println();
	}

	@Override
	boolean endOfGame() {
		return end;
	}

	@Override
	void printWinner() {
		System.out.println("The winner is player number " + winner);
		
	}

}

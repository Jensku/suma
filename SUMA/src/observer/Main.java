package observer;

import java.util.Observable;
import java.util.Observer;

public class Main {

	public static void main(String[] args) {
		Observable clocktimer = new ClockTimer(0, 0, 0);
		Observer digitalclock = new DigitalClock();
		clocktimer.addObserver(digitalclock);
		
		new Thread((Runnable)clocktimer).start();

	}

}

package observer;


import java.util.Observable;
public class ClockTimer extends Observable implements Runnable {
	
	private Thread clockThread = null;
	private volatile boolean ready = false;
	private int minute;
	private int second;
	private int hour;
	
	public ClockTimer(int hour, int minute, int second) {
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}
	
	public void start() {
		if (clockThread == null) {
			clockThread = new Thread(this, "Clock");
			clockThread.start();
		}
	}
	
	public void run() {
		String clock;
		while (!ready) {
			try {
				Thread.sleep(1000);
				second++;
				if(second % 60 == 0) {
					minute++;
					second = 0;
					if(minute % 60 == 0) {
						hour++;
						minute = 0;
					}
				}
				clock = hour + ":" + minute + ":" + second;
				setChanged();
				notifyObservers(clock);
			} catch (InterruptedException e) {
				System.out.println("Ei toimi");
			}
		}
	}
	
	public void stop() {
		ready= true;
	}

}

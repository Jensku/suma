package observer;

import java.util.Observable;
import java.util.Observer;

public class DigitalClock implements Observer {

	private String time = null;
	
	@Override
	public void update(Observable o, Object arg) {
		time = (String) arg;
		showTime(time);
	}
	
	public void showTime(String time) {
		System.out.println(time);
	}

}

package builder;

public class Waiter {
	private BurgerBuilder burgerBuilder;
	
	public void setBurgerBuilder(BurgerBuilder bb) {
		this.burgerBuilder = bb;
	}
	
	public void constructBurger() {
		burgerBuilder.createNewBurger();
		burgerBuilder.buildBuns();
		burgerBuilder.buildLettuce();
		burgerBuilder.buildPatty();
		burgerBuilder.buildSauce();
		burgerBuilder.buildCheese();
	}
}

package builder;

public class Hamburger {
	
	private String buns;
	private String lettuce;
	private String patty;
	private String sauce;
	private String cheese;
	
	public void setBuns(String buns) {
		this.buns = buns;
	}
	
	public void setLettuce(String lettuce) {
		this.lettuce = lettuce;
	}
	
	public void setPatty(String patty) {
		this.patty = patty;
	}
	
	public void setSauce(String sauce) {
		this.sauce = sauce;
	}
	
	public void setCheese(String cheese) {
		this.cheese = cheese;
	}
}

package builder;

public class Buns {
	
	private String name;
	
	public Buns() {
		name = "Sesame bun";
	}
	
	public String getName() {
		return name;
	}

}

package builder;

public class Patty {
	
	private String name;
	
	public Patty() {
		name = "Crispy chicken";
	}
	
	public String getName() {
		return name;
	}

}

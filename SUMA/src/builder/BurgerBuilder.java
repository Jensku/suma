package builder;

public abstract class BurgerBuilder {
	protected Hamburger burger;
	
	public void createNewBurger() {
		burger = new Hamburger();
	}
	
	public abstract void buildBuns();
	public abstract void buildLettuce();
	public abstract void buildPatty();
	public abstract void buildSauce();
	public abstract void buildCheese();
	public abstract void getBurger();

}

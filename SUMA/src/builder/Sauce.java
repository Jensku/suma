package builder;

public class Sauce {
	
	private String name;
	
	public Sauce() {
		name = "Ranch sauce";
	}
	
	public String getName() {
		return name;
	}

}

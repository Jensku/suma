package builder;

public class Lettuce {
	
	private String name;
	
	public Lettuce() {
		name = "Romaine lettuce";
	}
	
	public String getName() {
		return name;
	}

}

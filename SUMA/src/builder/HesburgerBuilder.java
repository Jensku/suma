package builder;

import java.util.ArrayList;
import java.util.List;

public class HesburgerBuilder extends BurgerBuilder {
	
	List<String> burger = new ArrayList<String>();
	Buns buns = new Buns();
	Lettuce lettuce = new Lettuce();
	Patty patty = new Patty();
	Sauce sauce = new Sauce();
	Cheese cheese = new Cheese();

	@Override
	public void buildBuns() {
		burger.add(buns.getName());
	}

	@Override
	public void buildLettuce() {
		burger.add(lettuce.getName());
	}

	@Override
	public void buildPatty() {
		burger.add(patty.getName());
	}

	@Override
	public void buildSauce() {
		burger.add(sauce.getName());
	}

	@Override
	public void buildCheese() {
		burger.add(cheese.getName());
	}
	
	public void getBurger() {
		for(int i = 0; i < burger.size(); i++) {
			System.out.print(burger.get(i) + ", ");
		}
	}
}

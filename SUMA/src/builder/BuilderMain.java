package builder;

public class BuilderMain {

	public static void main(String[] args) {
		
		Waiter waiter = new Waiter();
		BurgerBuilder hesburger = new HesburgerBuilder();
		BurgerBuilder mcdonalds = new McDonaldsBuilder();
		
		waiter.setBurgerBuilder(hesburger);
		waiter.constructBurger();
		
		System.out.println("Hesburger's menu:");
		hesburger.getBurger();
		System.out.println();
		
		waiter.setBurgerBuilder(mcdonalds);
		waiter.constructBurger();
		
		System.out.println("McDonald's' menu:");
		mcdonalds.getBurger();
	}

}

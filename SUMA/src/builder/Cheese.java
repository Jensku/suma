package builder;

public class Cheese {
	
	private String name;
	
	public Cheese() {
		name = "Blue cheese";
	}
	
	public String getName() {
		return name;
	}
}

package builder;

public class McDonaldsBuilder extends BurgerBuilder {
	
	StringBuilder sb = new StringBuilder();

	@Override
	public void buildBuns() {
		sb.append("Brioche bun, ");
	}

	@Override
	public void buildLettuce() {
		sb.append("Iceberg lettuce, ");
	}

	@Override
	public void buildPatty() {
		sb.append("Beef patty, ");
	}

	@Override
	public void buildSauce() {
		sb.append("Aioli, ");
	}

	@Override
	public void buildCheese() {
		sb.append("Cheddar");
	}
	
	public void getBurger() {
		System.out.print(sb.toString());
	}
}

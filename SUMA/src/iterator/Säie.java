package iterator;

import java.util.Iterator;
import java.util.LinkedList;

public class Säie extends Thread {
	
	private Iterator<String> iteraattori;
	private String str = "";
	
	public Säie(Iterator<String> iteraattori) {
		this.iteraattori = iteraattori;
	}

	@Override
	public void run() {
		synchronized(iteraattori) {
			while(iteraattori.hasNext()) {
				str = (String) iteraattori.next();
				System.out.println(this);
				System.out.println(iteraattori.next());
				
				if(str.equals("seppo")) {
					iteraattori.remove();
					System.out.println("Seppo poistettu :-(");
					break;
				}
			}
		}
	}

}

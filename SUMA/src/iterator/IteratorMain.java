package iterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class IteratorMain {

	public static void main(String[] args) throws InterruptedException {
		List<String> lista = new LinkedList<String>();
		lista.add("moi ");
		lista.add("mun ");
		lista.add("nimi ");
		lista.add("on ");
		lista.add("seppo");
		lista.add("taalasmaa");
		
		
		Iterator<String> iteraattori1 = lista.iterator();
		Iterator<String> iteraattori2 = lista.iterator();
		Iterator<String> iteraattori = lista.iterator();
		
		/*System.out.println("Kaksi säiettä, kaksi iteraattoria:");
		Säie säie1 = new Säie(iteraattori1);
		Säie säie2 = new Säie(iteraattori2);
		
		säie1.start();
		säie1.join();
		säie2.start();
		säie2.join();*/
		
		System.out.println("Kaksi säiettä, yksi iteraattori:");
		Säie säie3 = new Säie(iteraattori);
		Säie säie4 = new Säie(iteraattori);
		
		säie3.start();
		säie3.join();
		säie4.start();
		säie4.join();
	}
}

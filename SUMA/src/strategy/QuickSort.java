package strategy;

public class QuickSort implements Strategy {
	
	int[] sortee;

	@Override
	public void sort(int[] numbers) {
		sortee = numbers;
		System.out.println("\nGenerated numbers: " + sortee.length);
		quickSort(sortee, 0, sortee.length-1);
		System.out.println("QuickSort ready");
		
		for (int i = 0; i < sortee.length; i++) {
            System.out.print(sortee[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println("");
        }
	}
	
	 public static void quickSort(int table[], int lo0, int hi0) {
	        int lo = lo0;
	        int hi = hi0;
	        int mid, swap;

	        mid = table[ (lo0 + hi0) / 2];
	        while (lo <= hi) {
	            while (table[lo] < mid) {
	                ++lo;
	            }
	            
	            while (table[hi] > mid) {
	                --hi;
	            }
	            if (lo <= hi) {
	                swap = table[lo];
	                table[lo] = table[hi];
	                ++lo;
	                table[hi] = swap;
	                --hi;
	            }
	        }

	        if (lo0 < hi) {
	            quickSort(table, lo0, hi);
	        }
	        if (lo < hi0) {
	            quickSort(table, lo, hi0);
	        }
	  }
}

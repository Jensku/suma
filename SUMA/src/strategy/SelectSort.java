package strategy;

import java.util.Random;

public class SelectSort implements Strategy {
	
	int[] taul;
	int MAX;

	@Override
	public void sort(int[] numbers) {
		taul = numbers;
		MAX = taul.length;
		System.out.println("\nGenerated numbers: " + taul.length);
		selectSort();
		System.out.println("SelectSort ready");
		for (int i = 0; i < MAX; i++) {
            System.out.print(taul[i]+" ");
            if (i>0 && i%40==0) // rivinvaihto
                System.out.println("");
        }
		
	}
	
	public void selectSort() {
		 //taul tallettaa lajiteltavat tiedot
	    int i, j, k, apu, pienin;
        for (i=0;i<MAX;i++) {
            pienin=i; //sisempi for-lause alla
            for (j=i+1;j<MAX;j++) {
                /* löytyykö taulukon loppupäästä pienempää alkiota? *///if-lause alla
                if (taul[j] < taul[pienin]) {
                    pienin=j;
                }//sisempi for-lause
            }//if-lause alla
            if (pienin != i) {
                /* jos löytyi suoritetaan alkioiden vaihto */
                apu=taul[pienin];
                taul[pienin]=taul[i];
                taul[i]=apu;
            }//ulompi for-lause
        }
    }

}

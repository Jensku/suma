package strategy;

import java.util.Random;
import java.util.Scanner;

public class StrategyMain {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Sorter sorter = new Sorter(null);
		int MAX = 200000;
		int[] array = new int[MAX];
		Random rand = new Random();
		for (int i = 0; i < MAX; i++) {
            array[i] = rand.nextInt(1000);
        }
		
		System.out.println("Choose how to sort:\n (1) Merge sort\n (2) Quick sort\n (3) Select sort");
		int choice = scanner.nextInt();
		
		if(choice == 1) {
			sorter.setStrategy(new MergeSort());
			sorter.arrange(array);
		} else if(choice == 2) {
			sorter.setStrategy(new QuickSort());
			sorter.arrange(array);
		} else if(choice == 3) {
			sorter.setStrategy(new SelectSort());
			sorter.arrange(array);
		}else {
			System.out.println("Invalid choice");
		}
	}
}

package strategy;


public class Sorter {
	private Strategy strategy;
	
	public Sorter(Strategy s) {
		this.strategy = s;
	}
	
	public void arrange(int[] input) {
		strategy.sort(input);
	}
	
	public void setStrategy(Strategy s) {
		this.strategy = s;
	}
	
}
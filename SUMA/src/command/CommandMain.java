package command;

public class CommandMain {
	
	public static void main(String[] args) {
		Screen screen = new Screen();
		Command screenUp = new ScreenUpCommand(screen);
		Command screenDown = new ScreenDownCommand(screen);
		ButtonUp buttonUp = new ButtonUp(screenUp);
		ButtonDown buttonDown = new ButtonDown(screenDown);
		
		buttonUp.press();
		buttonDown.press();
	}

}

package command;

public class Screen {
	
	public void screenUp() {
		System.out.println("Screen is up. No movies for you.");
	}
	
	public void screenDown() {
		System.out.println("Screen is down. What shall we watch?");
	}

}

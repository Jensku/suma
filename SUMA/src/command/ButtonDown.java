package command;

public class ButtonDown {
Command command;
	
	public ButtonDown(Command command) {
		this.command = command;
	}
	
	public void press() {
		command.execute();
	}

}

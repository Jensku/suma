package command;

public class ButtonUp {
	Command command;
	
	public ButtonUp(Command command) {
		this.command = command;
	}
	
	public void press() {
		command.execute();
	}

}

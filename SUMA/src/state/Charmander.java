package state;

import java.util.Random;

public class Charmander extends State {
	
	public Charmander() {
		System.out.println("Charmander");
	}

	@Override
	public void attack(Pokemon p) {
		System.out.println("Attack: 52");
	}

	@Override
	public void defence(Pokemon p) {
		System.out.println("Defence: 43");
	}

	@Override
	public void hp(Pokemon p) {
		System.out.println("HP: 39");
	}

	@Override
	public void exp(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charmander just evolved to ");
			p.changeState(new Charmeleon());
		} else {
			System.out.println("Your Charmander didn't evolve! :-(");
			
		}
	}
		
	
}

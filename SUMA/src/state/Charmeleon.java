package state;

import java.util.Random;

public class Charmeleon extends State {
	
	public Charmeleon() {
		System.out.println("Charmeleon");
	}

	@Override
	public void attack(Pokemon p) {
		System.out.println("Attack: 64");
	}

	@Override
	public void defence(Pokemon p) {
		System.out.println("Defence: 58");
	}

	@Override
	public void hp(Pokemon p) {
		System.out.println("HP: 58");
	}

	@Override
	public void exp(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charmeleon just evolved to ");
			p.changeState(new Charizard());
		} else {
			System.out.println("Your Charmeleon just devolved to ");
			p.changeState(new Charmander());
		}
	}
}


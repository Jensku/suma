package state;

import java.util.Random;

public class Charizard extends State {
	
	public Charizard() {
		System.out.println("Charizard");
	}

	@Override
	public void attack(Pokemon p) {
		System.out.println("Attack: 84");
	}

	@Override
	public void defence(Pokemon p) {
		System.out.println("Defence: 78");
	}

	@Override
	public void hp(Pokemon p) {
		System.out.println("HP: 78");
	}

	@Override
	public void exp(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charizard can't evolve any further");
		} else {
			System.out.println("Your Charizard just devolved to ");
			p.changeState(new Charmeleon());
		}
		
	}

}

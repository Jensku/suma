package proxy;

public class RealImage implements Image {
	
	private final String filename;
	
	public RealImage(String filename) {
		this.filename = filename;
	}

	private void loadImageFromDisk() {
		System.out.println("Loading " + filename);
	}

	public void displayImage() {
		loadImageFromDisk();
		System.out.println("Displaying " + filename);
	}

	@Override
	public void showData() {
		
	}

}

package proxy;

import java.util.ArrayList;
import java.util.List;

public class ProxyMain {
	public static void main(String[] args) {
		
		Image flower = new ProxyImage("flower.jpg");
		Image dog = new ProxyImage("dog.jpg");
		Image cat = new ProxyImage("cat.jpg");
		
		List<Image> photoAlbum = new ArrayList<Image>();
		photoAlbum.add(flower);
		photoAlbum.add(dog);
		photoAlbum.add(cat);
		
		for(Image image : photoAlbum) {
			image.showData();
		}
		
		for(Image image : photoAlbum) {
			image.displayImage();
		}
	}
}

package composite;

public class Verkkokortti implements Laiteosa {

	double hinta;
	
	public Verkkokortti() {
		hinta = 20;
	}
	
	@Override
	public double naytaHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(Laiteosa osa) {
		throw new RuntimeException
		("Ei voi lisätä osia.");
		
	}

}

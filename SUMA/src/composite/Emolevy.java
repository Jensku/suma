package composite;

import java.util.ArrayList;
import java.util.List;

public class Emolevy implements Laiteosa {
	
	double hinta;
	List<Laiteosa> emolevynOsat = new ArrayList<>();
	
	public Emolevy() {
		hinta = 186.90;
	}

	@Override
	public double naytaHinta() {
		for(Laiteosa laiteosa: emolevynOsat) {
			hinta += laiteosa.naytaHinta();
		}
		return hinta;
	}

	@Override
	public void lisaaOsa(Laiteosa osa) {
		emolevynOsat.add(osa);
		
	}

}

package composite;

public class Prosessori implements Laiteosa {

	double hinta;
	
	public Prosessori() {
		hinta = 149.90;
	}
	
	@Override
	public double naytaHinta() {
		// TODO Auto-generated method stub
		return hinta;
	}

	@Override
	public void lisaaOsa(Laiteosa osa) {
		throw new RuntimeException
		("Ei voi lisätä osia.");
		
	}

}

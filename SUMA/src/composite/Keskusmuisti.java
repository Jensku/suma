package composite;

public class Keskusmuisti implements Laiteosa {

	double hinta;
	
	public Keskusmuisti() {
		hinta = 37.90;
	}
	@Override
	public double naytaHinta() {
		return hinta;
	}

	@Override
	public void lisaaOsa(Laiteosa osa) {
		throw new RuntimeException
		("Ei voi lisätä osia.");
		
	}

}

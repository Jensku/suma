package composite;

public interface Laiteosa {
	double naytaHinta();
	void lisaaOsa(Laiteosa osa);
}

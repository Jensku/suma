package composite;

import java.util.ArrayList;
import java.util.List;

public class Kotelo implements Laiteosa {
	
	double hinta;
	List<Laiteosa> listaOsa = new ArrayList<>();
	
	public Kotelo() {
		hinta = 159.90;
	}

	@Override
	public double naytaHinta() {
		for(Laiteosa laiteosa: listaOsa) {
			hinta += laiteosa.naytaHinta();
		}
		return hinta;
	}

	@Override
	public void lisaaOsa(Laiteosa osa) {
		listaOsa.add(osa);
	}
}

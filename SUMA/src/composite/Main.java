package composite;

public class Main {

	public static void main(String[] args) {
		Laiteosa kotelo = new Kotelo();
		Laiteosa emolevy = new Emolevy();
		Laiteosa keskusmuisti = new Keskusmuisti();
		Laiteosa naytonohjain = new Naytonohjain();
		Laiteosa prosessori = new Prosessori();
		Laiteosa verkkokortti = new Verkkokortti();
		
		emolevy.lisaaOsa(keskusmuisti);
		emolevy.lisaaOsa(prosessori);
		
		kotelo.lisaaOsa(emolevy);
		kotelo.lisaaOsa(verkkokortti);
		kotelo.lisaaOsa(naytonohjain);
		
		System.out.println("Koneen hinnaksi tuli " + kotelo.naytaHinta() + "€");
	}
}

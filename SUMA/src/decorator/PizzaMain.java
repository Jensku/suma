package decorator;

public class PizzaMain {

	public static void main(String[] args) {
		
		Pizza kinkkupizza = new Tomaattikastike (new Kinkku (new Ananas (new Juusto (new Pizzapohja()))));
		Pizza margherita = new Tomaattikastike ( new Juusto (new Pizzapohja()));
		Pizza kasvispizza = new Tomaattikastike (new Oliivi (new Ananas (new Herkkusieni (new Juusto (new Pizzapohja())))));
		
		System.out.println(kinkkupizza.kuvaus() + ", " + kinkkupizza.hinta() + "€");
		System.out.println(margherita.kuvaus() + ", " + margherita.hinta() + "€");
		System.out.println(kasvispizza.kuvaus() + ", " + kasvispizza.hinta() + "€");
	}

}

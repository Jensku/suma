package decorator;

public class Tomaattikastike extends Taytteet {
	
	public Tomaattikastike(Pizza taytettavaPizza) {
		super(taytettavaPizza);
	}

	@Override
	public String kuvaus() {
		return super.kuvaus() + ", Tomaattikastike";
	}
	
	@Override
	public double hinta() {
		return super.hinta() + 1.95;
	}

}

package decorator;

public abstract class Taytteet implements Pizza {

	private Pizza taytettavaPizza;
	
	public Taytteet(Pizza taytettavaPizza) {
		this.taytettavaPizza = taytettavaPizza;
	}
	
	public String kuvaus() {
		return taytettavaPizza.kuvaus();
	}
	public double hinta() {
		return taytettavaPizza.hinta();
	}

}

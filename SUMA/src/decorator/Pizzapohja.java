package decorator;

public class Pizzapohja implements Pizza {

	@Override
	public String kuvaus() {
		return "Rapea pizzapohja";
	}

	@Override
	public double hinta() {
		return 1.50;
	}
	
	
}

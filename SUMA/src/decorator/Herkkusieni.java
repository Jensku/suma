package decorator;

public class Herkkusieni extends Taytteet {
	
	public Herkkusieni(Pizza taytettavaPizza) {
		super(taytettavaPizza);
	}

	@Override
	public String kuvaus() {
		return super.kuvaus() + ", Herkkusieni";
	}
	
	@Override
	public double hinta() {
		return super.hinta() + 2.50;
	}
}
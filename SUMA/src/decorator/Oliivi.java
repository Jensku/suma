package decorator;

public class Oliivi extends Taytteet {
		
		public Oliivi(Pizza taytettavaPizza) {
			super(taytettavaPizza);
		}
		
		@Override
		public String kuvaus() {
			return super.kuvaus() + ", Oliivi";
		}
		
		@Override
		public double hinta() {
			return super.hinta() + 2.95;
		}
}

package decorator;

public class Juusto extends Taytteet {
	
	public Juusto(Pizza taytettavaPizza) {
		super(taytettavaPizza);
	}
	
	@Override
	public String kuvaus() {
		return super.kuvaus() + ", Juusto";
	}
	
	@Override
	public double hinta() {
		return super.hinta() + 2.15;
	}

}

package decorator;

public class Kinkku extends Taytteet {
	
	public Kinkku(Pizza taytettavaPizza) {
		super(taytettavaPizza);
	}
	
	@Override
	public String kuvaus() {
		return super.kuvaus() + ", Kinkku";
	}
	
	@Override
	public double hinta() {
		return super.hinta() + 2.75;
	}
}
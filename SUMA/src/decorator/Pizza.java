package decorator;

public interface Pizza {
	public String kuvaus();
	public double hinta();
	
}

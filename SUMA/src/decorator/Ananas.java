package decorator;

public class Ananas extends Taytteet {
	
	public Ananas(Pizza taytettavaPizza) {
		super(taytettavaPizza);
	}
	
	@Override
	public String kuvaus() {
		return super.kuvaus() + ", Ananas";
	}
	
	@Override
	public double hinta() {
		return super.hinta() + 1.75;
	}

}

package chainofresponsibility;

public class CEO extends SalaryHandler {
	
	private final int percentage = 5;
	
	public void processRequest(SalaryRequest request) {
		if(request.getAmount() > percentage) {
			System.out.println("The CEO will approve " + request.getAmount() + "% payrise.");
		} else {
			System.out.println("Your request for " + request.getAmount() + "% needs a board meeting!");
		}
	}

}

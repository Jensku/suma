package chainofresponsibility;

import java.util.Scanner;

public class SalaryMain {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		Foreperson foreperson = new Foreperson();
		HeadOfUnit headofunit = new HeadOfUnit();
		CEO ceo = new CEO();
		
		foreperson.setSuccessor(headofunit);
		headofunit.setSuccessor(ceo);
		
		System.out.println("Enter the payrise in percentage to check who should approve it.");
		int r = scanner.nextInt();
		foreperson.processRequest(new SalaryRequest(r));
	}
}

package chainofresponsibility;

public abstract class SalaryHandler {
	
	private SalaryHandler successor;
	
	
	public void setSuccessor(SalaryHandler successor) {
		this.successor = successor;
	}
	
	public void processRequest(SalaryRequest request) {
		if(successor != null) {
			successor.processRequest(request);
		}
	}
}

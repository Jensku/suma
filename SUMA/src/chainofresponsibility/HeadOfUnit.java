package chainofresponsibility;

public class HeadOfUnit extends SalaryHandler {
	private final int percentageMin = 2;
	private final int percentageMax = 5;
	 
	public void processRequest(SalaryRequest request) {
		if(request.getAmount() > percentageMin && request.getAmount() <= percentageMax) {
			System.out.println("Head of unit will approve " + request.getAmount() + "% payrise.");
		} else {
			super.processRequest(request);
		}
	}
	
}

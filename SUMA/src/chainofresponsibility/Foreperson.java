package chainofresponsibility;

public class Foreperson extends SalaryHandler {
	
	private final int percentage = 2;
	
	
	public void processRequest(SalaryRequest request) {
		if(request.getAmount() <= percentage) {
			System.out.println("Foreperson will approve " + request.getAmount() + "% payrise.");
		} else {
			super.processRequest(request);
		}
	}

}

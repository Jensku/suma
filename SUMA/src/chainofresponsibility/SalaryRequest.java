package chainofresponsibility;

public class SalaryRequest {
	
	private int amount;
	
	public SalaryRequest(int amount) {
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
}

package facade;

public class Eläinkauppa {
	private Eläin pupu;
	private Eläin hamsteri;
	private Eläin käärme;
	
	public Eläinkauppa() {
		pupu = new Pupu();
		hamsteri = new Hamsteri();
		käärme = new Käärme();
	}
	
	public void ruokiPupu() {
		pupu.ostaRuokaa();
	}
	
	public void ruokiHamsteri() {
		hamsteri.ostaRuokaa();
	}
	
	public void ruokiKäärme() {
		käärme.ostaRuokaa();
	}

}

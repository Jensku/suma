package facade;

public class FacadeMain {

	public static void main(String[] args) {
		Eläinkauppa eläinkauppa = new Eläinkauppa();
		
		System.out.println("Tervetuloa eläinkauppaan!");
		eläinkauppa.ruokiHamsteri();
		eläinkauppa.ruokiKäärme();
		eläinkauppa.ruokiPupu();

	}

}

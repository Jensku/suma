package facade;

public class Käärme implements Eläin {

	@Override
	public void ostaRuokaa() {
		System.out.println("Käärmeille annetaan käärmeenruokaa");
	}

}

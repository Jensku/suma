package memento;

import java.util.Random;

public class Arvuuttaja {
	
	private volatile int numero;
	Random random = new Random();
	
	public synchronized boolean arvaaNumero(Object object, int arvaus) {
		return ((Memento) object).getNumero() == arvaus;
	}
	
	public synchronized Object liityPeliin() {
		numero = random.nextInt(10) + 1;
		return new Memento(numero);
	}
	
	private class Memento {
		private int numero;
		
		public Memento(int numero) {
			this.numero = numero;
		}
		
		public int getNumero() {
			return numero;
		}
	}
}

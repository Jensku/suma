package memento;

import java.util.Random;

public class Asiakas extends Thread {
	
	Random rand = new Random();
	private Object object;
	boolean end = false;
	private Arvuuttaja arvuuttaja;
	int arvaus;
	
	public Asiakas(Arvuuttaja arvuuttaja) {
		this.arvuuttaja = arvuuttaja;
	}
	
	public void liityPeliin() {
		this.object = this.arvuuttaja.liityPeliin();
	}
	
	public void arvaaNumero() {
		arvaus = rand.nextInt(10) + 1;
		arvuuttaja.arvaaNumero(object, arvaus);
	}
	
	public void run() {
		
		int i;
		Object j = arvuuttaja.liityPeliin();
		Boolean b = false;
		while(!b) {
			i = rand.nextInt(10) + 1;
			System.out.println("Arvasin " + this + " " + i);
			b = arvuuttaja.arvaaNumero(j, i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Oikein arvattu!");
	}
}

package memento;

public class MementoMain {

	public static void main(String[] args) {
		
		Arvuuttaja arvuuttaja = new Arvuuttaja();
		Asiakas[] asiakas = new Asiakas[3];
		
		for(int i = 0; i < asiakas.length; i++) {
			asiakas[i] = new Asiakas(arvuuttaja);
			asiakas[i].liityPeliin();
			asiakas[i].start();
		}
	}
}

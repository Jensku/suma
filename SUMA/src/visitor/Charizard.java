package visitor;

import java.util.Random;

public class Charizard extends State {
	
	PokemonVisitor visitor = new PokemonVisitor();
	
	public Charizard() {
		System.out.println("Charizard");
	}

	@Override
	public void attack(Pokemon p) {
		System.out.println("Attack: 84");
	}

	@Override
	public void defence(Pokemon p) {
		System.out.println("Defence: 78");
	}

	@Override
	public void hp(Pokemon p) {
		System.out.println("HP: 78");
	}

	@Override
	public void exp(Pokemon p) {
		visitor.expCharizard(p);
		
	}

}

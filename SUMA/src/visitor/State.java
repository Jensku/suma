package visitor;

public abstract class State {
	
	public abstract void attack(Pokemon p);
	public abstract void defence(Pokemon p);
	public abstract void hp(Pokemon p);
	public abstract void exp(Pokemon p);
	public void changeState(Pokemon p, State s) {
		p.changeState(s);
	}
}

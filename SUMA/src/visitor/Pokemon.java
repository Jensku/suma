package visitor;

public class Pokemon {
	private State state;
	
	public Pokemon() {
		state = new Charmander();
	}
	
	public void changeState(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}
	
	public void exp() {
		state.exp(this);
	}
	
	public void attack() {
		state.attack(this);
	}
	
	public void defence() {
		state.defence(this);
	}
	
	public void hp() {
		state.hp(this);
	}
	
}

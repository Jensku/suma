package visitor;

import java.util.Random;

public class PokemonVisitor implements Visitor {
	
	public void expCharmander(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charmander just evolved to ");
			p.changeState(new Charmeleon());
		} else {
			System.out.println("Your Charmander didn't evolve :-(");
		}
	}
	
	public void expCharmeleon(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charmeleon just evolved to ");
			p.changeState(new Charizard());
		} else {
			System.out.println("Your Charmeleon just devolved to ");
			p.changeState(new Charmander());
		}
	}
	
	public void expCharizard(Pokemon p) {
		Random number = new Random();
		int rand = number.nextInt(100);
		if(rand > 50) {
			System.out.println("Your Charizard can't evolve any further");
		} else {
			System.out.println("Your Charizard just devolved to ");
			p.changeState(new Charmeleon());
		}
	}
}

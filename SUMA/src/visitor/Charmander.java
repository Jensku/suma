package visitor;



public class Charmander extends State {
	
	PokemonVisitor visitor = new PokemonVisitor();
	
	public Charmander() {
		System.out.println("Charmander");
	}

	@Override
	public void attack(Pokemon p) {
		System.out.println("Attack: 52");
	}

	@Override
	public void defence(Pokemon p) {
		System.out.println("Defence: 43");
	}

	@Override
	public void hp(Pokemon p) {
		System.out.println("HP: 39");
	}

	@Override
	public void exp(Pokemon p) {
		visitor.expCharmander(p);
	}
}

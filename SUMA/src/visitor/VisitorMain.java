package visitor;

import java.util.Scanner;

public class VisitorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Pokemon pokemon = new Pokemon();
		pokemon.attack();
		pokemon.defence();
		pokemon.hp();
	
		System.out.println("Feed some exp berries to your Pokemon for a chance to evolve. Careful, it might also devolve!");
		System.out.println("How many berries?");
		int answer = scanner.nextInt();
		
		for(int i = 0; i < answer; i++) {
			pokemon.exp();
			pokemon.attack();
			pokemon.defence();
			pokemon.hp();
		}
		
	}

}

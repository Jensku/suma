package visitor;

public interface Visitor {
	
	public void expCharmander(Pokemon p);
	public void expCharmeleon(Pokemon p);
	public void expCharizard(Pokemon p);

}

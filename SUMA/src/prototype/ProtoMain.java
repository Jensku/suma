package prototype;

public class ProtoMain {

	public static void main(String[] args) {
		
		Viisari tuntiviisari = new Viisari(15);
		Viisari minuuttiviisari = new Viisari(41);
		Viisari sekuntiviisari = new Viisari(16);
		
		Kello kello = new Kello(tuntiviisari, minuuttiviisari, sekuntiviisari);
		
		System.out.println("Kello on: " + kello.printKello());
		
		Kello kloonikello = (Kello) kello.clone();
		System.out.println("Kloonikello näyttää: " + kloonikello.printKello());
		
		kello.getTunti().setAika(17);
		kello.getMinuutti().setAika(26);
		kello.getSekunti().setAika(43);
		System.out.println("Kellon uusi aika: " + kello.printKello());
		
		kloonikello.getTunti().setAika(22);
		kloonikello.getMinuutti().setAika(52);
		kloonikello.getSekunti().setAika(56);
		System.out.println("Kloonikellon uusi aika: " + kloonikello.printKello());

	}

}

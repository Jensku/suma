package prototype;

public class Kello implements Cloneable {
	
	private Viisari sekunti;
	private Viisari minuutti;
	private Viisari tunti;
	
	public Viisari getTunti() {
		return tunti;
	}
	
	public Viisari getMinuutti() {
		return minuutti;
	}
	
	public Viisari getSekunti() {
		return sekunti;
	}
	
	public Kello(Viisari t, Viisari m, Viisari s) {
		this.tunti = t;
		this.minuutti = m;
		this.sekunti = s;
	}
	/*
	// matalakopio
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Kloonaus ei onnistunut.");
			return null;
		}
	}*/
	
	// syväkopio
	public Kello clone() {
		Kello k = null;
		try {
			k = (Kello) super.clone();
			k.tunti = (Viisari) tunti.clone();
			k.minuutti = (Viisari) minuutti.clone();
			k.sekunti = (Viisari) sekunti.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Syväkopio ei onnistunut");
		}
		return k;
	}
	
	public String printKello() {
		return getTunti() + ":" + getMinuutti() + ":" + getSekunti();
	}
}

package prototype;

public class Viisari implements Cloneable {
	private int aika;
	
	public Viisari(int aika) {
		this.aika = aika;
	}
	
	public int getAika() {
		return aika;
	}
	
	public void setAika(int aika) {
		this.aika = aika;
	}
	
	@Override
	public String toString() {
		return String.valueOf(aika);
	}
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Viisarikopio ei onnistunut");
			e.printStackTrace();
		}
		return null;
	}
}

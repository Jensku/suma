package adapter;

public class DogAdapter implements ToyDog {
	
	Dog dog;
	public DogAdapter(Dog dog) {
		this.dog = dog;
	}

	@Override
	public void squeak() {
		dog.makeSound();
	}
}

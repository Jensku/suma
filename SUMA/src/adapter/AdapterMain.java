package adapter;

public class AdapterMain {

	public static void main(String[] args) {
		
		ElviTheDog Elvi = new ElviTheDog();
		ToyDog toy = new SqueakyToyDog();
		
		ToyDog dogAdapter = new DogAdapter(Elvi);
		
		System.out.println("Elvi does:");
		Elvi.guard();
		Elvi.makeSound();
		
		System.out.println("Toy dog does:");
		toy.squeak();
		
		System.out.println("Dog adapter does:");
		dogAdapter.squeak();
	}

}

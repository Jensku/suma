package adapter;

public interface Dog {
	
	public void guard();
	public void makeSound();

}

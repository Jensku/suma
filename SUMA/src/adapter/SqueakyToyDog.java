package adapter;

public class SqueakyToyDog implements ToyDog {

	@Override
	public void squeak() {
		System.out.println("Beep beep");
	}
}

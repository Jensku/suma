package adapter;

public class ElviTheDog implements Dog {

	@Override
	public void guard() {
		System.out.println("Snooping and guarding");
	}

	@Override
	public void makeSound() {
		System.out.println("Bork bork");
	}

}

package abstractfactory;

public class BossShirt implements Outfit {

	String style;
	
	 public BossShirt() {
		 this.style = "dress shirt";
	 }

	@Override
	public String toString() {
		return style;
	}
}

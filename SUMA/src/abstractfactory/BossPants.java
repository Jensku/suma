package abstractfactory;

public class BossPants implements Outfit{

	String style;
	
	 public BossPants() {
		 this.style = "tailored pants";
	 }

	@Override
	public String toString() {
		return style;
	}

}

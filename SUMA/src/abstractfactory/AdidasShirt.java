package abstractfactory;

public class AdidasShirt implements Outfit {
	
	String style;
	
	 public AdidasShirt() {
		 this.style = "Adidas t-shirt";
	 }

	@Override
	public String toString() {
		return style;
	}

}

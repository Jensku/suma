package abstractfactory;

public class AdidasFactory implements AbstractOutfitFactory {

	@Override
	public Outfit makeShoes() {
		return new AdidasShoes();
	}

	@Override
	public Outfit makePants() {
		return new AdidasPants();
	}

	@Override
	public Outfit makeShirt() {
		return new AdidasShirt();
	}

	@Override
	public Outfit makeHat() {
		return new AdidasHat();
	}

}

package abstractfactory;

import java.util.ArrayList;

public class OutfitApp {
	public static void main(String[] args) {
		
		System.out.println("Jasper is a student and always dresses in Adidas.");
		AbstractOutfitFactory outfitFactory = OutfitPicker.getFactory("Adidas");
		System.out.println(JaspersOutfit.createOutfit(outfitFactory));
			
		System.out.println("Finally Jasper has graduated and can now afford better things in life!");
		outfitFactory = OutfitPicker.getFactory("Boss");
		System.out.println(JaspersOutfit.createOutfit(outfitFactory));
	}
}
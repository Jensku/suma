package abstractfactory;

public class AdidasShoes implements Outfit{

	String style;
	
	 public AdidasShoes() {
		 this.style = "Adidas sneakers";
	 }

	@Override
	public String toString() {
		return style;
	}
}

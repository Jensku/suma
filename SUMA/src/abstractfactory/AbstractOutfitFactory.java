package abstractfactory;

public interface AbstractOutfitFactory {
	Outfit makeShoes();
	Outfit makePants();
	Outfit makeShirt();
	Outfit makeHat();

}

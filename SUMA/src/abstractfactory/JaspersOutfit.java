package abstractfactory;

public class JaspersOutfit {
	public static String createOutfit(AbstractOutfitFactory factory) {
		Outfit shoes = factory.makeShoes();
		Outfit pants = factory.makePants();
		Outfit shirt = factory.makeShirt();
		Outfit hat = factory.makeHat();
		return "Jasper is wearing " + shoes + ", " + pants + ", " + shirt + ", " + "and " + hat + ".";
	}
}

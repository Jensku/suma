package abstractfactory;

public class BossHat implements Outfit {

	String style;
	
	 public BossHat() {
		 this.style = "Hugo Boss cap";
	 }

	@Override
	public String toString() {
		return style;
	}

}

package abstractfactory;

public class AdidasPants implements Outfit {

	String style;
	
	 public AdidasPants() {
		 this.style = "jeans";
	 }

	@Override
	public String toString() {
		return style;
	}
}

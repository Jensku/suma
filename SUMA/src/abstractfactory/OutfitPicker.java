package abstractfactory;

public class OutfitPicker {
	public static AbstractOutfitFactory getFactory(String choice) {
		if("Adidas".equalsIgnoreCase(choice)) {
			return new AdidasFactory();
		} else if("Boss".equalsIgnoreCase(choice)) {
			return new BossFactory();
		}
		return null;
	}

}

package abstractfactory;

public class AdidasHat implements Outfit {

	String style;
	
	 public AdidasHat() {
		 this.style = "Adidas cap";
	 }

	@Override
	public String toString() {
		return style;
	}
}

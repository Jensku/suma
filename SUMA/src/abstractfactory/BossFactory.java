package abstractfactory;

public class BossFactory implements AbstractOutfitFactory {

	@Override
	public Outfit makeShoes() {
		return new BossShoes();
	}

	@Override
	public Outfit makePants() {
		return new BossPants();
	}

	@Override
	public Outfit makeShirt() {
		return new BossShirt();
	}

	@Override
	public Outfit makeHat() {
		return new BossHat();
	}

}

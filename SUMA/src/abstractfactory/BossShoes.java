package abstractfactory;

public class BossShoes implements Outfit {

	String style;
	
	 public BossShoes() {
		 this.style = "Hugo Boss shoes";
	 }

	@Override
	public String toString() {
		return style;
	}

}
